<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\AunzScoreHeader;
use App\Http\Models\AunzScoreResponse;
use App\Http\Models\Metric;
use App\Http\Models\AunzMongo;
use Validator;
use Input;
use Redirect;
use Session;
use View;
use Auth;
use Datatables;
use DB;

class AunzController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('aunz.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agent_options      = array('' => 'Choose One') + DB::table('agents')->lists('name','id');
        $metrics            = Metric::where('campaign_id','=',2)->where('is_complicance','=',0)->where('is_auto_fail','=',0)->orderBy('id', 'asc')->get();
        $metrics_compliance = Metric::where('campaign_id','=',2)->where('is_complicance','=',1)->where('is_auto_fail','=',0)->orderBy('id', 'asc')->get();
        $metrics_red_flag   = Metric::where('campaign_id','=',2)->where('is_auto_fail','=',1)->where('is_complicance','=',0)->orderBy('id', 'asc')->get();

        return view('aunz.create')->with(array('agent_options'=>$agent_options, 'metrics'=>$metrics, 'metrics_compliance' => $metrics_compliance, 'metrics_red_flag' =>$metrics_red_flag));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $rules = array(
            'agent_id'          => 'required',
            'call_details'      => 'required',
            'campaign'          => 'required',
            'assessor_name'     => 'required',
            'disposition'       => 'required',
            'amount'            => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            return Redirect::to('aunz/create')->withInput()->withErrors($validator);
        }
        else
        {
                $header = new AunzScoreHeader();
                $header->agent_id               = Input::get('agent_id');
                $header->call_details           = Input::get('call_details');
                $header->campaign               = Input::get('campaign');
                $header->assessor_name          = Input::get('assessor_name');
                $header->disposition            = Input::get('disposition');
                $header->amount                 = Input::get('amount');
                $header->remarks                = Input::get('remarks');
                $header->recommendation         = Input::get('recommendation');
                $header->assesment_comments     = Input::get('assesment_comments');
                $header->compliance_comments    = Input::get('compliance_comments');
                $header->metrics_score          = Input::get('metrics_score');
                $header->compliance_score       = Input::get('compliance_score');
                $header->redflag_score          = Input::get('redflag_score');

                $header->submitted_by_id        = Auth::user()->id;
                $header->save();

                $aunz_score_header_id   = $header->id;
                $metrics_id             = Input::get('metrics_id');
                $tl_assessment          = Input::get('tl_assessment');
                $score                  = Input::get('score');

                /** Start Mongo DB Code */
                // $responses_arr = array();
                // $mongo = new AunzMongo();
                // $mongo->header_id              = $aunz_score_header_id;
                // $mongo->agent_id               = Input::get('agent_id');
                // $mongo->call_details           = Input::get('call_details');
                // $mongo->campaign               = Input::get('campaign');
                // $mongo->assessor_name          = Input::get('assessor_name');
                // $mongo->disposition            = Input::get('disposition');
                // $mongo->amount                 = Input::get('amount');
                // $mongo->remarks                = Input::get('remarks');
                // $mongo->recommendation         = Input::get('recommendation');
                // $mongo->assesment_comments     = Input::get('assesment_comments');
                // $mongo->compliance_comments    = Input::get('compliance_comments');
                // $mongo->metrics_score          = Input::get('metrics_score');
                // $mongo->compliance_score       = Input::get('compliance_score');
                // $mongo->redflag_score          = Input::get('redflag_score');
                // $mongo->submitted_by_id        = Auth::user()->id;
                 /** End Mongo DB Code */

                foreach($metrics_id as $key => $value) {
                    $responses = new AunzScoreResponse();
                    $responses->aunz_score_header_id    = $aunz_score_header_id;
                    $responses->metrics_id              = $value;
                    $responses->tl_assessment           = $tl_assessment[$key];
                    $responses->score                   = $score[$key];
                    $responses->save();

                    // Start Make array for responses to be saved in mongo db
                    // $arrayName = array(
                    //     'aunz_score_header_id' => $aunz_score_header_id, 
                    //     'metrics_id'           => $value, 
                    //     'tl_assessment'        => $tl_assessment[$key], 
                    //     'score'                => $score[$key], 
                    // );

                    // array_push($responses_arr, $arrayName);
                    // End make array for mongo DB
                }

                // $mongo->responses  = $responses_arr; // Save to MongoDB
                // $mongo->save();

            Session::flash('alert-success', 'Form Submitted.');
            return Redirect::to('aunz/create');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAunzDash()
    {
        $from = Input::get('from');
        $to = Input::get('to');
        $data = DB::table('aunz_score_header')
        ->join('agents', 'agents.id', '=', 'aunz_score_header.agent_id')
        ->select('aunz_score_header.id', 'aunz_score_header.assessor_name', 'aunz_score_header.amount', 'aunz_score_header.metrics_score', 'aunz_score_header.compliance_score', 'aunz_score_header.compliance_score' ,'agents.name as agent_name')
        ->where('aunz_score_header.created_at', '>=', $from)
        ->where('aunz_score_header.created_at', '<', $to)
        ->get();

        return json_encode($data);
    }

}   