<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Models\Metric;
use Validator;
use Input;
use Redirect;
use Session;
use View;
use Auth;
use Datatables;
use DB;

class MetricsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        return view('metrics.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
          'criteria'        => 'required',
          'description'     => 'required',
          'question'        => 'required',
      );

      $validator = Validator::make(Input::all(), $rules);
      if ($validator->fails())
      {
          return Redirect::to('metrics/create')->withErrors($validator);
      }
      else
      {
          $metrics = new Metric();
          $metrics->criteria            = Input::get("criteria");
          $metrics->description         = Input::get("description");
          $metrics->question            = Input::get("question");
          $metrics->campaign_id         = Input::get("campaign_id");
          $metrics->potential_points    = Input::get("potential_points");
          $metrics->is_complicance      = Input::get("is_complicance");
          $metrics->is_auto_fail        = Input::get("is_auto_fail");
          
          

          if($metrics->save())
          {
              Session::flash('alert-success', 'Form Submitted Successfully.');
          }
          
          else

          {
              Session::flash('alert-danger', 'Form Submitted Successfully.');
          }

          return Redirect::to('metrics/create');
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}