<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Models\Metric;
use App\Http\Models\UkScoreHeader;
use App\Http\Models\UkScoreResponse;
use DB;
use Auth;
use Validator;
use Redirect;
use Input;
use Session;

class UkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('uk.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $agent_options      = array('' => 'Choose One') + DB::table('agents')->lists('name','id');
        $metrics            = Metric::where('campaign_id','=',1)->where('is_complicance','=',0)->orderBy('id', 'asc')->get();
        $metrics_compliance = Metric::where('campaign_id','=',1)->where('is_complicance','=',1)->orderBy('id', 'asc')->get();
        $metrics_red_flag   = Metric::where('campaign_id','=',1)->where('is_auto_fail','=',1)->where('is_complicance','=',0)->orderBy('id', 'asc')->get();

        return view('uk.create')->with(array('agent_options'=>$agent_options, 'metrics'=>$metrics, 'metrics_compliance' => $metrics_compliance, 'metrics_red_flag' => $metrics_red_flag));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'agent_id'          => 'required',
            'assesment_date'    => 'required',
            'actual_call_date'  => 'required',
            'time_of_call'      => 'required',
            'qa_type'           => 'required',
            'assessor_name'     => 'required',
            'call_reason'       => 'required',
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            return Redirect::to('uk/create')->withInput()->withErrors($validator);
        }
        else
        {

            $header = new UkScoreHeader();
            $header->agent_id               = Input::get('agent_id');
            $header->assesment_date         = Input::get('assesment_date');
            $header->actual_call_date       = Input::get('actual_call_date');
            $header->time_of_call           = Input::get('time_of_call');
            $header->qa_type                = Input::get('qa_type');
            $header->assessor_name          = Input::get('assessor_name');
            $header->call_reason            = Input::get('call_reason');
            $header->assesment_comments     = Input::get('assesment_comments');
            $header->compliance_comments    = Input::get('compliance_comments');
            $header->metrics_score          = Input::get('metrics_score');
            $header->compliance_score       = Input::get('compliance_score');
            $header->redflag_score          = Input::get('redflag_score');
            $header->submitted_by_id        = Auth::user()->id;
            $header->save();

            $Uk_score_header_id = $header->id;

            $metrics_id     = Input::get('metrics_id');
            $tl_assessment  = Input::get('tl_assessment');
            $score          = Input::get('score');

            foreach ($metrics_id as $key => $value) {
                $responses = new UkScoreResponse();
                $responses->Uk_score_header_id = $Uk_score_header_id;
                $responses->metrics_id          = $value;
                $responses->tl_assessment       = $tl_assessment[$key];
                $responses->score               = $score[$key];
                $responses->save();
            }

            Session::flash('alert-success', 'Form Submitted.');
            return Redirect::to('uk/create');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
     public function getUkDash()
    {
        $from = Input::get('from');
        $to = Input::get('to');
        $data = DB::table('uk_score_header')
        ->join('agents', 'agents.id', '=', 'uk_score_header.agent_id')
        ->select('uk_score_header.id', 'uk_score_header.assessor_name', 'uk_score_header.actual_call_date', 'uk_score_header.metrics_score', 'uk_score_header.compliance_score', 'uk_score_header.compliance_score' ,'agents.name as agent_name')
        ->where('uk_score_header.created_at', '>=', $from)
        ->where('uk_score_header.created_at', '<', $to)
        ->get();

        return json_encode($data);
    }
}