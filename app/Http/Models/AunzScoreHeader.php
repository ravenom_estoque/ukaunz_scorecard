<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class AunzScoreHeader extends Model
{
    protected $table = 'aunz_score_header';
}
