<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class AunzScoreResponse extends Model
{
    protected $table = 'aunz_score_responses';
}
