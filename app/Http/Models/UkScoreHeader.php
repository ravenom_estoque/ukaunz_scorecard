<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class UkScoreHeader extends Model
{
    protected $table = 'uk_score_header';
}
