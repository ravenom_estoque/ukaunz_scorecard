<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class UkScoreResponse extends Model
{
    protected $table = 'uk_score_responses';
}
