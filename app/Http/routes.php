<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function () {
    return view('home');
});

Route::get('home', function () {
    return view('home');
});

Route::get('config', function () {
    return view('config');
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/* Score Card Route */
Route::resource('uk', 'UkController');
Route::resource('metrics', 'MetricsController');
Route::resource('aunz', 'AunzController');

/* Dashboard Route */
Route::get('dashboard-aunz', 'AunzController@getAunzDash');
Route::get('dashboard-uk', 'UkController@getUkDash');