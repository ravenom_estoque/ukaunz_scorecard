@extends('app')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Metrics Form</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('home') }}">Home</a>
            </li>
            <li class="active">
                <strong>Metrics Form</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><small>Metrics Form</small></h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a> 
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">

                            <form class="form-horizontal" id="payment-form" role="form" method="POST" action="{{ url('metrics') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="flash-message">
                                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                      @if(Session::has('alert-' . $msg))
                                      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
                                      @endif
                                    @endforeach
                                </div>

                                <fieldset>
                                    <legend>Metrics</legend>
                                      <div class="form-group">
                                            <label  class="col-sm-2 control-label">Criteria</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" name="criteria">
                                            </div>
                                      </div>
                                      <div class="form-group">
                                            <label  class="col-sm-2 control-label">Description</label>
                                            <div class="col-lg-10">
                                                <textarea class="form-control" id="desc" name="description"></textarea>
                                            </div>
                                      </div>
                                      <div class="form-group">
                                            <label  class="col-sm-2 control-label">Question</label>
                                            <div class="col-lg-10">
                                                <textarea class="form-control" id="ques" name="question"></textarea>
                                            </div>
                                      </div>
                                      <div class="form-group">
                                            <label  class="col-sm-2 control-label">Campaign</label>
                                            <div class="col-lg-10">
                                                <select class="form-control" name="campaign_id">
                                                    <option value="">Choose One</option>
                                                    <option value="1">UK</option>
                                                    <option value="2">AUNZ</option>
                                                </select>
                                            </div>
                                      </div>
                                      <div class="form-group">
                                            <label  class="col-sm-2 control-label">Potential Points</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" name="potential_points">
                                            </div>
                                      </div>
                                      <div id="A" class="form-group">
                                            <label  class="col-sm-2 control-label">For Compliance?</label>
                                            <div class="col-lg-10">
                                                <select id="A1" class="form-control" name="is_complicance">
                                                    <option value="">Choose One</option>
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </div>
                                      </div>

                                      <div id="B" class="form-group">
                                            <label  class="col-sm-2 control-label">Is Auto Fail</label>
                                            <div class="col-lg-10">
                                                <select id="B1" class="form-control" name="is_auto_fail">
                                                    <option value="">Choose One</option>
                                                    <option value="1">Yes</option>
                                                    <option value="0">No</option>
                                                </select>
                                            </div>
                                      </div>
                                </fieldset>
                                

                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('metrics-create')
<script>
$('#desc, #ques').summernote();
$("#A1").change(function() {
     var val = $(this).val();

     if (val == 1){
        $("#B").hide();
    }  else {
        $("#B").show();
    }

});

 $("#B1").change(function() {
     var val = $(this).val();

     if (val == 1){
        $("#A").hide();
    }  else {
        $("#A").show();
    }

});

</script>           
@endsection


