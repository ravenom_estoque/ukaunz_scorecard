@extends('app')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>UK Score Card Form</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('home') }}">Home</a>
            </li>
            <li class="active">
                <strong>UK Score Card Form</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><small>UK Score Card Form</small></h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a> 
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">

                            <form class="form-horizontal" id="payment-form" role="form" method="POST" action="{{ url('uk') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="flash-message">
                                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                                      @if(Session::has('alert-' . $msg))
                                      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }}</p>
                                      @endif
                                    @endforeach
                                </div>

                                <fieldset>

                                    <legend>Score Card Details</legend>
                                    
                                      <div class="form-group">
                                        <label class="col-sm-2 control-label" for="textinput">Agent Name</label>
                                        <div class="col-sm-10">
                                          {!! Form::select('agent_id', $agent_options, '',array('class' => 'form-control', 'id' => 'agent_id', 'required' => 'required')) !!}       
                                        </div>
                                      </div>

                                      <div class="form-group">
                                            <label  class="col-sm-2 control-label">Assessment Date</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" name="assesment_date" id="assesment_date">
                                            </div>
                                      </div>

                                      <div class="form-group">
                                            <label  class="col-sm-2 control-label">Actual Call Date</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" name="actual_call_date" id="actual_call_date">
                                            </div>
                                      </div>

                                      <div class="form-group">
                                            <label  class="col-sm-2 control-label">Time of Call</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" name="time_of_call">
                                            </div>
                                      </div>

                                      <hr>

                                       <div class="form-group">
                                            <label  class="col-sm-2 control-label">QA Type</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" name="qa_type">
                                            </div>
                                      </div>

                                      <div class="form-group">
                                            <label  class="col-sm-2 control-label">Assessor Name</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" name="assessor_name">
                                            </div>
                                      </div>

                                      <div class="form-group">
                                            <label  class="col-sm-2 control-label">Call Reason</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" name="call_reason">
                                            </div>
                                      </div>
                            
                                </fieldset>

                                <fieldset>
                                    <legend>Metrics</legend>
                                    <table class="table">
                                        <thead>
                                          <tr>
                                            <th>Criteria</th>
                                            <th>Description</th>
                                            <th>Assessment Question</th>
                                            <th>TL Assessment</th>
                                            <th>Potential Points</th>
                                            <th>Score %</th>
                                          </tr>
                                        </thead>

                                        @foreach($metrics as $key => $value)
                                            <tr>
                                                <td class="col-md-1">{{$value->criteria}}</td>
                                                <td class="col-md-3">{!!$value->description!!}</td>
                                                <td class="col-md-3">{!!$value->question!!}</td>
                                                <td class="col-md-2">
                                                    <select class="form-control ts_assessment count_total" data-key="test" name="tl_assessment[]">
                                                        <option value=""></option>
                                                        <option value="Yes">Yes</option>
                                                        <option value="No">No</option>
                                                        <option value="N/A">N/A</option>
                                                    </select>
                                                </td>
                                                <td class="col-md-2 potential">{{number_format($value->potential_points, 2)}}</td>
                                                <td class="col-md-4">
                                                    <input type="text" class="form-control score" name="score[]">
                                                    <input type="hidden" class="form-control" name="metrics_id[]" value="{{$value->id}}">
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </fieldset>

                                <fieldset>
                                    <div class="form-group">
                                            <label  class="col-sm-2 control-label">Comments</label>
                                            <div class="col-lg-10">
                                                <textarea class="form-control" name="assesment_comments"></textarea>
                                            </div>
                                      </div>
                                </fieldset>

                                <fieldset>
                                    <div class="form-group">
                                            <label  class="col-sm-2 control-label">Score</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" name="metrics_score" id="metrics_score" value="0" />
                                            </div>
                                      </div>
                                </fieldset>

                                <fieldset>
                                    <legend>Compliance Assessment</legend>
                                    <table class="table">
                                        <thead>
                                          <tr>
                                            <th>Criteria</th>
                                            <th>Interpretation</th>
                                            <th>Examples</th>
                                            <th>TL Assessment</th>
                                            <th>Potential Points</th>
                                            <th>Score %</th>
                                          </tr>
                                        </thead>

                                        @foreach($metrics_compliance as $key => $value)
                                            <tr>
                                                <td class="col-md-1">{{$value->criteria}}</td>
                                                <td class="col-md-3">{!!$value->description!!}</td>
                                                <td class="col-md-3">{!!$value->question!!}</td>
                                                <td class="col-md-2">
                                                    <select class="form-control ts_assessment compliance_count" data-key="test" name="tl_assessment[]">
                                                        <option value=""></option>
                                                        <option value="Yes">Yes</option>
                                                        <option value="No">No</option>
                                                        <option value="N/A">N/A</option>
                                                    </select>
                                                </td>
                                                <td class="col-md-2 potential">{{number_format($value->potential_points, 2)}}</td>
                                                <td class="col-md-4">
                                                    <input type="text" class="form-control score" name="score[]">
                                                    <input type="hidden" class="form-control" name="metrics_id[]" value="{{$value->id}}">
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </fieldset>

                                <fieldset>
                                    <div class="form-group">
                                            <label  class="col-sm-2 control-label">Comments</label>
                                            <div class="col-lg-10">
                                                <textarea class="form-control" name="compliance_comments"></textarea>
                                            </div>
                                      </div>
                                </fieldset>

                                <fieldset>
                                    <div class="form-group">
                                            <label  class="col-sm-2 control-label">Score</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" name="compliance_score" id="compliance_score" value="0" />
                                            </div>
                                      </div>
                                </fieldset>

                                <fieldset>
                                    <legend>Red Flag Auto Fail</legend>
                                    <table class="table">
                                        <thead>
                                          <tr>
                                            <th>Criteria</th>
                                            <th>Interpretation</th>
                                            <th>Examples</th>
                                            <th>TL Assessment</th>
                                            <th>Potential Points</th>
                                            <th>Score %</th>
                                          </tr>
                                        </thead>

                                        @foreach($metrics_red_flag as $key => $value)
                                            <tr>
                                                <td class="col-md-1">{{$value->criteria}}</td>
                                                <td class="col-md-3">{!!$value->description!!}</td>
                                                <td class="col-md-3">{!!$value->question!!}</td>
                                                <td class="col-md-2">
                                                    <select class="form-control fail-assesment redflag_count" data-key="test" name="tl_assessment[]">
                                                        <option value=""></option>
                                                        <option value="N/A">Not Applicable</option>
                                                        <option value="Yes">Yes</option>
                                                    </select>
                                                </td>
                                                <td class="col-md-2 fail-points">{{number_format($value->potential_points, 2)}}</td>
                                                <td class="col-md-4">
                                                    <input type="text" class="form-control fail-score" name="score[]">
                                                    <input type="hidden" class="form-control" name="metrics_id[]" value="{{$value->id}}">
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </fieldset>

                                

                                <fieldset>
                                    <div class="form-group">
                                            <label  class="col-sm-2 control-label">Comments</label>
                                            <div class="col-lg-10">
                                                <textarea class="form-control" name="autofail_comments"></textarea>
                                            </div>
                                      </div>
                                </fieldset>

                                <fieldset>
                                    <div class="form-group">
                                            <label  class="col-sm-2 control-label">Score</label>
                                            <div class="col-lg-10">
                                                <input type="text" class="form-control" name="redflag_score" id="redflag_score" value="0" />
                                            </div>
                                      </div>
                                </fieldset>
                                
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('uk-create')
<script>
$(document).ready(function() {

    $('.ts_assessment').change(function() {
        var val = $(this).val();
        var parent = $(this).parents('tr');

        if (val == 'Yes' || val == 'N/A') {
            var potential = parent.find('.potential').text();
            parent.find('.score').val(potential);               
        } else {
            parent.find('.score').val('0');
        }
     });

    $('#assesment_date, #actual_call_date').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "yyyy-mm-dd"
    });

    $('.fail-assesment').change(function() {
        var val = $(this).val();
        var parent = $(this).parents('tr');

        if (val == 'Yes') {
            var potential = parent.find('.fail-points').text();
            parent.find('.fail-score').val(potential);               
        } else {
            parent.find('.fail-score').val('0');
        }
     });

}); 

</script>           
@endsection