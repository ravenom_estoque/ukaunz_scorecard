@extends('app')
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>UK Dashboard</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('home') }}">Home</a>
            </li>
            <li class="active">
                <strong>UK Dashboard</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>UK</small></h5>
                    <div class="ibox-tools">

                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                       
                        
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>

                    </div>
                </div>
                <div class="ibox-content">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-4 control-label">From Date</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="fromDateAll" id="fromDateAll" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">To Date</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="toDateAll" id="toDateAll" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="button" class="btn btn-primary" id="btnReport1">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>UK</small></h5>
                    <div class="ibox-tools">

                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                       
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>

                    </div>
                </div>
                <div class="ibox-content">
                    <table id="UkTbl" class="table table-striped table-bordered" cellspacing="0" width="100%">

                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Agent</th>
                                <th>Assesor Name</th>
                                <th>Actual Call Date</th>
                                <th>Metrics Score</th>
                                <th>Compliance Score</th>
                                <th>Redflag Score</th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('uk-index')
<script>
$(document).ready(function(){
$('#fromDateAll, #toDateAll').datepicker({
    keyboardNavigation: false,
    forceParse: false,
    autoclose: true,
    format: "yyyy-mm-dd"
});

$("#btnReport1").click(function(){
    var from = $("#fromDateAll").val();
    var to   = $("#toDateAll").val();

    if($.fn.dataTable.isDataTable('#UkTbl'))
    {
        console.log("Destroy");
        t.destroy();
        t.clear().draw();
    }

    $.ajax({
        url: "dashboard-uk", 
        type: 'GET',
        data: {'from' : from, 'to' : to},
        success: function(result){
        var myObj = $.parseJSON(result);
        $.each(myObj, function(key,value) {
            var t = $('#UkTbl').DataTable();
            t.row.add( [
                value.id,
                value.agent_name,
                value.assessor_name,
                value.actual_call_date,
                value.metrics_score,
                value.compliance_score,
                value.compliance_score
            ] ).draw();
        });
    }});

});
   
   
}); 
</script>           
@endsection
